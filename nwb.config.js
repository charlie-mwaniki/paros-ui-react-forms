module.exports = {
    type: "react-component",
    babel: require("./env/.babelrc.json"),
    npm: {
        esModules: false,
        umd: false
    }
}
