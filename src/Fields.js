import React from "react";
import withRef from "./hoc/withRef";
import isEqual from "lodash/isEqual";

import { getHookTemplate, mergeProperties, IS_ARRAY_ADAPTER, clone } from "./utils";
import MetadataContext from "./MetadataContext";

const IGNORED_PROPS = [
    "_cachedFields", "form"
];

const getFieldArray = fields =>
    (Array.isArray(fields) ? fields : [fields]).filter(x => x && x.length);

const getEmptyProps = (obj = {}, prop) => {
    if(Array.isArray(obj)) {
        let arr = getFieldArray(obj);
        return arr.reduce((obj, field) => ({ ...obj, [field]: null }), {});
    } else if(typeof obj === "object") {
        return Object.keys(obj).reduce((obj, field) => ({ ...obj, [field]: null }), {});
    } else return getEmptyProps([obj], prop);
};

class FormFields {
    _cachedFields = {};

    constructor(form) {
        this.form = form;
    }

    // FormFields, "company"
    get(self, prop) {
        if(IGNORED_PROPS.includes(prop)) return self[prop];

        if(this._cachedFields[prop])
            return this._cachedFields[prop];

        let form = this.form;

        this._cachedFields[prop] = withRef(class FormWrappedFieldComponent extends React.PureComponent {
            static contextType = MetadataContext;

            state = getEmptyProps(this.props.monitors, prop);
            monitors = clone(this.props.monitors || {});
            fieldRequires = getEmptyProps(this.props.fieldRequires, prop);
            fieldExcludedBy = getEmptyProps(this.props.fieldExcludedBy, prop);

            // Disables/enables this field if the target is falsy
            requireValueFor = (field, invert = false, target = "fieldRequires") => {
                let handler = ({ value, previousValue, forceUpdate }) => {
                    if(isEqual(value, previousValue) && !forceUpdate) return;

                    if(!!value ^ invert) { //XOR
                        form.enableField(prop);
                    } else {
                        form.disableField(prop);
                    }
                };

                this[target][field] = handler;
                form.onChange(field, handler);

                if(!form.value(field)) {
                    form.disableField(prop);
                }
            }

            // Updates state if target changes with either string-based or function-based mapping
            createMonitor = field => {
                let mapping = this.props.monitors[field];
                let func = typeof mapping === "function" ? mapping : val => ({ [mapping]: val });

                let handler = ({ value }) => {
                    let obj = func(value);
                    this.setState(obj);
                };

                this.monitors[field] = handler;
                form.onChange(field, handler);
            }

            onUpdate = () => this.forceUpdate();

            componentDidMount() {
                let { objectProp } = this.context || {};
                let key = mergeProperties(objectProp, prop);

                form.addField(key);
                form.onChange(key, this.onUpdate);

                if(typeof this.monitors === "object") {
                    Object.keys(this.monitors).forEach(field => this.createMonitor(field));
                } else if(Array.isArray(this.monitors)) {
                    let monitors = [...this.monitors];
                    this.monitors = {};
                    monitors.map(field => this.createMonitor(field));
                } else if(typeof this.monitors === "string") {
                    let monitor = this.monitors;
                    this.monitors = {};
                    this.createMonitor(monitor);
                }

                Object.keys(this.fieldRequires).forEach(field => this.requireValueFor(field));
                Object.keys(this.fieldExcludedBy).forEach(field => this.requireValueFor(field, true, "fieldExcludedBy"));
            }

            componentWillUnmount() {
                let { objectProp } = this.context || {};
                let key = mergeProperties(objectProp, prop);

                Object.keys(this.fieldExcludedBy).forEach(field => {
                    form.offChange(field, this.fieldExcludedBy[field]);
                    delete this.fieldExcludedBy[field];
                });

                Object.keys(this.fieldRequires).forEach(field => {
                    form.offChange(field, this.fieldRequires[field]);
                    delete this.fieldRequires[field];
                });

                Object.keys(this.monitors).forEach(field => {
                    form.offChange(field, this.monitors[field]);
                    delete this.monitors[field];
                });

                form.offChange(key, this.onUpdate);
                form.removeField(key);
            }

            render() {
                let { Component, ref, template, props } = getHookTemplate(prop, this.context, this.props);
                let { hidden, ...hook } = form.hookField(template);

                if(hidden) return null;

                if(Component[IS_ARRAY_ADAPTER]) {
                    return (
                        <Component {...hook} {...props} {...this.state} template={template} hook={hook} ref={ref} form={form} monitors={this.monitors} />
                    );
                } else {
                    return (
                        <Component {...hook} {...props} {...this.state} ref={ref} />
                    );
                }

            }
        });

        return this._cachedFields[prop];
    }
}

/**
 * @returns {ReactForm.FormFields}
 */
export default impl => {
    let fields = new FormFields(impl);
    return new Proxy(fields, fields);
};
