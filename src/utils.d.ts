import * as React from "react";
import ReactForm from "./ReactForm";

interface GetHookTemplateOpts<T> {
    component: React.ReactInstance;
    valueProp?: string;
    valueGetter?: string | Function;
    onChange?: Function;
    onBlur?: Function;
    defaultValue?: any;
    componentProps?: object;
    template?: string;
    name?: string;
    forwardedRef?: React.Ref<T>;
    validateOnBlur?: boolean;
}

interface Template {
    name: string;
    valueProp: string;
    valueGetter: string | Function;
    onChange: Function;
    onBlur: Function;
    defaultValue: any;
}

interface HookTemplate<T extends React.ReactInstance> {
    Component: T;
    ref: React.Ref<T>;
    props: {};
    template: {};
}

export function mergeProperties(...props: string[]): string;

export function getHookTemplate<T extends React.ReactInstance>(field: string, context: {}, fieldProps: GetHookTemplateOpts<T>): HookTemplate<T>;

export function determineValueGetter(valueGetter: string | Function): Function;

export function validateForm<T>(form: ReactForm, values: T): Promise<{ error?: T, value?: T }>;

export function clone<T>(obj: T): T;
