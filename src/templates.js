import ArrayFieldAdapter from "./ArrayFieldAdapter";

let _templates = {};

// Mutate output, template, templateProps to modify field properties!
// { field, output, template, templateProps, context, props }
const templates = {
    // The most basic field type, a component that directly returns a value
    "component": ({ template }) => {
        template.valueGetter = v => v;
    },

    // Really simple textbox
    "text": ({ output, template, templateProps }) => {
        if(!output.Component) {
            output.Component = "input";
        }

        templateProps.type = "text";
        template.forbidNull = true;
    },

    // Really simple multiline textbox
    "textarea": ({ output, template, templateProps }) => {
        if(!output.Component) {
            output.Component = "textarea";
        }

        template.forbidNull = true;
    },

    // Really simple checkbox
    "checkbox": ({ output, template, templateProps }) => {
        if(!output.Component) {
            output.Component = "input";
        }

        template.valueGetter = "target.checked";
        template.valueProp = "checked";
        templateProps.type = "checkbox";
        template.defaultValue = false;
        template.forbidNull = true;
    },

    // react-select integration
    "select": ({ output, template, props, templateProps }) => {
        // if(!output.Component) {
        //     output.Component = Select;
        // }

        templateProps.isDisabled = props.disabled || props.isDisabled; //Standardize `disabled` prop for all input types
        template.defaultValue = null;
        template.valueGetter = v => v;
    },

    // ArrayFieldAdapter integration for modifying arrays of data
    "array": ({ field, output, template, templateProps, props }) => {
        const Component = output.Component;
        output.Component = ArrayFieldAdapter;

        // TODO: For some reason, this is the only way we can properly set static properties on the ArrayFieldAdapter component.
        output.Component[IS_ARRAY_ADAPTER] = true;

        templateProps.field = field;
        templateProps.fieldComponent = Component;
        templateProps.fieldProps = props.componentProps;
        templateProps.valueGetter = props.valueGetter || "target.value";
        templateProps.valueProp = props.valueProp || "value";
        templateProps.onChangeProp = props.onChangeProp || "onChange";

        template.defaultValue = [];
        template.valueProp = "value";
        template.onChangeProp = "onChange";
        template.valueGetter = v => v;

        //TODO: Support templates in ArrayFieldAdapter children
    }
};

/**
 * @param {{ [template: string]: (options: Object) => void }} additions
 */
export const addTemplates = (additions) => {
    Object.keys(additions).forEach(name => {
        if(!_templates[name]) _templates[name] = [];

        _templates[name].push(additions[name]);
    });
}

export const getTemplate = name => {
    if(!_templates[name]) return x => x;

    return obj => _templates[name].reduce((obj, func) => func(obj) || obj, obj);
}

addTemplates(templates);

export default {
    addTemplates,
    getTemplate
};
