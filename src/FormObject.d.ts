import * as React from "react";

interface FormObjectProps {
    field: string;
    children: any;
}

interface FormObject extends React.FunctionComponent<FormObjectProps> {}
export = FormObject;