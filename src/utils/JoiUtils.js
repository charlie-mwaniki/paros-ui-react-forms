const getPart = (schema, prop) => {
    if(!schema.schema) return null;

    let parts = prop.split(".");

    let root = schema;
    for(let part of parts) {
        let segment = root._ids._byKey.get(part);
        if(!segment) return null;

        root = segment;
    }

    return root;
};

const getMessages = schema => {
    if(schema._preferences && schema._preferences.messages) {
        return Object.keys(schema._preferences.messages).reduce((msgs, key) => ({
            ...msgs,
            [key]: schema._preferences.messages[key].source
        }), {});
    }

    return {};
};

/**
 * Validate a single property of an object schema
 * @param {import("joi").ObjectSchema} schema
 * @param {string} prop
 * @param {any} value
 * @returns {import("joi").ValidationResult}
 */
export const validatePartial = (schema, prop, value) => {
    let part = getPart(schema, prop);
    if(!part) return { error: null, value }; //If not existent in the schema, should we ignore it?

    part = part.schema;
    let partMessages = getMessages(part);
    let schemaMessages = getMessages(schema);

    part = part.messages(schemaMessages)
        .messages(partMessages);

    if(schema._preferences && schema._preferences.messages) {
        let messages = Object.keys(schema._preferences.messages).reduce((msgs, key) => ({
            ...msgs,
            [key]: schema._preferences.messages[key].source
        }), {});

        part = part.messages(messages);
    }

    return part.validate(value);
};

export default {
    validatePartial
};
