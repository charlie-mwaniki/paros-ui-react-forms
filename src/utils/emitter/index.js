export default function createEmitter() {
    let listeners = new Map();

    const on = (event, handler) => {
        if(!listeners.has(event)) {
            listeners.set(event, new Set());
        }

        listeners.get(event).add(handler);
    };

    const off = (event, handler) => {
        if(!listeners.has(event)) return false;

        return listeners.get(event).delete(handler);
    };

    const once = (event, handler) => {
        let wrapper = (...args) => {
            off(event, wrapper);
            return handler(...args);
        };

        on(event, wrapper);
    };

    const emit = (event, ...args) => {
        if(listeners.has(event) && listeners.get(event).size) {
            listeners.get(event).forEach(handler => handler(...args));
            return true;
        }

        return false;
    };

    return { on, off, once, emit };
}
