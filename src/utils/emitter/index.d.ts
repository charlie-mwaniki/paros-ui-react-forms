export interface Emitter {
    /**
     * Subscribe to an event
     * @param event Event name
     * @param callback Event handler
     */
    on(event: string, callback: Function): void;

    /**
     * Unsubscribe from an event
     * @param event Event name
     * @param callback Event handler
     * @returns True if existed and successfully removed
     */
    off(event: string, callback: Function): boolean;

    /**
     * Subscribe to the first instance of the event that occurs
     * @param event Event name
     * @param callback Event handler
     */
    once(event: string, callback: Function): void;

    /**
     * Publish an event to the emitter
     * @param event Event name
     * @param args Event data
     */
    emit(event: string, ...args: any[]): void;
}

/**
 * Naive and lightweight event emitter implementation. Way smaller than webpack's `events` shim
 */
export default function createEmitter(): Emitter;
