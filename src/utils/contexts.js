import React from "react";

const DefaultPassthroughHandler = (a, b) => ({ ...a, ...b });

/**
 * Creates a React context that can propagate its value through nested Providers
 * Unclear exactly how many updates this will create. Must investigate.
 * @template T
 * @param {T} defaultValue 
 * @param {*} calculateChangedBits 
 * @param {(a: any, b: any) => any} merge 
 * @returns {React.Context<T>}
 */
export const createNestingContext = (defaultValue, calculateChangedBits, merge = DefaultPassthroughHandler) => {
    const CTX = React.createContext(defaultValue, calculateChangedBits);
    const Provider = CTX.Provider;

    const PropagatingProvider = ({ value, merge: innerMerge = merge, children }) => (
        <CTX.Consumer>
            {passthrough => (
                <Provider value={innerMerge(passthrough, value)}>
                    {children}
                </Provider>
            )}
        </CTX.Consumer>
    );

    CTX.Provider = PropagatingProvider;

    return CTX;
};

export default {
    createNestingContext
};