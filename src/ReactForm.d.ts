import * as React from "react";
import { ReactFormValidation } from "./common";
import { EventEmitter } from "events";

import Form from "./Form";
import FormObject from "./FormObject";
import FieldWatcher from "./FieldWatcher";

export interface ReactFormOpts {
    name?: string;

    onSubmit?: Function;
    onChange?: Function;
    onError?: Function;
    onUpdate?: Function;

    debugUpdates?: boolean;
    debugChanges?: boolean;

    validateOnBlur?: boolean;
    validation?: ReactFormValidation;

    initialValues?: {};
    initialErrors?: {};
    disabledFields?: Array<String>;
}

interface HookFieldArgs {
    /**
     * Field to hook
     */
    target: string;
    /**
     * If internal value is null, set internal value to defaultValue
     * @default false
     */
    forbidNull?: boolean;
    defaultValue?: string | any;
    validateOnBlur?: boolean;
    value?: any;
    /**
     * @default "target.value"
     */
    valueGetter?: string | Function;
    /**
     * @default "value"
     */
    valueProp?: string;
    /**
     * @default "error"
     */
    errorProp?: string;
    /**
     * @default "onChange"
     */
    onChangeProp?: string;
    /**
     * @default "onBlur"
     */
    onBlurProp?: string;
    /**
     * Called after internal onChange unless interceptOnChange is true
     */
    onChange?: Function;
    /**
     * Called after internal onBlur
     */
    onBlur?: Function;
    /**
     * Props to pass directly to component
     */
    props?: {};
    /**
     * Call passed onChange before internal onChange fires
     * @default false
     */
    interceptOnChange?: boolean;
}

declare class ReactForm extends EventEmitter {
    name: string;
    validateOnBlur: boolean;
    validation: ReactFormValidation;

    Form: Form;
    Fields: { [field: string]: React.Component; }
    FormObject: FormObject;
    FieldWatcher: FieldWatcher;

    constructor(opts: ReactFormOpts);

    onChange(...listeners: Function[]): void;
    offChange(...listeners: Function[]): void;
    
    onSubmit(...listeners: Function[]): void;
    offSubmit(...listeners: Function[]): void;
    
    hookField(options: HookFieldArgs): {};
    addField(target: string): void;
    removeField(target: string): void;

    disableField(target: string): void;
    enableField(target: string): void;
    setDisabled(...fields: string[]): void;
    setEnabled(...fields: string[]): void;

    disable(): void;
    enable(): void;

    getState(): { errors: {}, values: {}, disabled: [] };

    values(): {};
    values(...fields: string[]): Array<any>;
    values(values: {}): void;

    errors(): {};
    errors(...fields: string[]): Array<any>;
    errors(errors: {}): void;

    value(field: string): any;
    value(field: string, value: any): void;

    error(field: string): any;
    error(field: string, value: any): void;

    replaceValues(values: {}, initial?: boolean): void;
    replaceErrors(errors: {}, initial?: boolean): void;

    resetValues(): void;
    resetErrors(): void;
    resetForm(): void;

    setState(values?: {}, errors?: {}, disabledFields?: []): void;

    validate(field: string, next?: (error: any, field: string) => void): Promise<void>;

    change(field: string, callback?: (field: string) => void): void;
    update(field: string): boolean;

    hookSubmit(e: Event): Promise<void>;
}

export default ReactForm;
