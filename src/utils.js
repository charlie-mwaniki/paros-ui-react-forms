import ReactFormFieldTemplates from "./templates";

import omit from "lodash/omit";
import get from "lodash/get";
import set from "lodash/set";
import joi from "joi";

// Because _.get will return null or undefined, we can't do standard truthy/falsy or === undefined operations.
// This lets us conveniently check if something genuinely isn't defined because Symbol() generates a unique symbol every time
export const UNDEFINED = Symbol("undefined");

// Used by ArrayFieldAdapter to selectively handle validation for subsections of a row
export const ARRAY_INDEX = Symbol("array_index");

export const IS_ARRAY_ADAPTER = Symbol("array_adapter");

export const mergeProperties = (...props) =>
    props.filter(x => x && x.length).join(".");

export const clone = obj => {
    if(obj instanceof Map) return new Map(obj);
    if(obj instanceof Set) return new Set(obj);

    if(Array.isArray(obj)) return [ ...obj ];
    if(typeof obj === "object") return { ...obj };

    return obj;
};

export const getPropValue = (obj, prop) => get(obj, prop, UNDEFINED);

export const propertyExists = (obj, prop) => getPropValue(obj, prop) !== UNDEFINED;

export const getHookTemplate = (field, context = {}, fieldProps) => {
    let { objectProp } = context;
    let {
        component: Component,
        valueGetter = "target.value",
        valueProp = "value",
        errorProp = "error",
        onChangeProp = "onChange",
        onBlurProp = "onBlur",
        onChange, onBlur,
        defaultValue,
        componentProps = {},
        template,
        forwardedRef,
        validateOnBlur,
        interceptOnChange = false,
        fieldHiddenWhenDisabled = false,
        ...props
    } = fieldProps;

    let _template = {
        name: field,
        defaultValue,
        valueGetter: determineValueGetter(valueGetter),
        valueProp, errorProp, onChangeProp, onBlurProp,
        onChange, onBlur,
        validateOnBlur,
        target: mergeProperties(objectProp, field),
        props: componentProps,
        hideWhenDisabled: fieldHiddenWhenDisabled,
        interceptOnChange
    };

    let res = { Component, ref: forwardedRef, props: omit(props, "name", "monitors", "fieldRequires"), template: _template };
    let transformer = typeof template === "string" && ReactFormFieldTemplates.getTemplate(template);

    if(transformer) {
        transformer({
            field: field,
            output: res,
            template: _template,
            templateProps: _template.props,
            context: context,
            props: fieldProps
        });
    } else if(typeof template === "function") {
        template({
            field: field,
            output: res,
            template: _template,
            templateProps: _template.props,
            context: context,
            props: fieldProps
        });
    }

    return res;
};

export const determineValueGetter = (valueGetter) => {
    if(typeof valueGetter === "function") {
        return valueGetter;
    } else if(typeof valueGetter === "string") {
        return e => get(e, valueGetter);
    } else {
        return e => get(e, "target.value", "");
    }
};

export const validateForm = async(form, values) => {
    /** @type {import("joi").ObjectSchema}  */
    let validation = form.validation;

    if(joi.isSchema(validation)) {
        let { error, value } = validation.validate(values, {
            allowUnknown: true,
            abortEarly: false,
            errors: { wrap: { label: "" } },
            messages: {
                "string.base": "{{#label}} is required",
                "string.empty": "{{#label}} is required",
                "string.pattern.base": "{{#label}} must be valid"
            }
        });

        if(error) {
            let errors = {};

            for(let err of error.details) {
                set(errors, err.path, err.message);
            }

            return { error: errors, value };
        } else {
            return { error: null, value };
        }
    }

    if(typeof validation === "function") {
        let errors = {};
        let promise = validation(values, errors);

        if(promise && promise.then) await promise;

        return { error: errors, value: values };
    }

    if(!validation)
        return { error: null, value: values };

    throw new Error("Invalid type for validation. Must be function or joi schema");
};
