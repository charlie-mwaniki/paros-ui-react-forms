import { validateForm, propertyExists, clone } from "./utils";
import createEmitter from "./utils/emitter";
import joi from "joi";

import get from "lodash/get";
import set from "lodash/set";
import unset from "lodash/unset";
import uniq from "lodash/uniq";

import setupForm from "./Form";
import setupFields from "./Fields";
import setupFieldWatcher from "./FieldWatcher";
import FormObject from "./FormObject";
import LogicBindings from "./logic";

export default class ReactForm {
    #errors = {};
    #values = {};
    #initialValues = {};
    #initialErrors = {};

    #fields = [];
    #disabled = [];

    name = "form";
    validateOnBlur = true;
    validation = joi.object();

    constructor({
        name = "form",

        onSubmit,
        onChange,
        onError,
        onUpdate,

        debugUpdates = !!localStorage.getItem("form:debug:updates"),
        debugChanges = !!localStorage.getItem("form:debug:changes"),

        validateOnBlur = true,
        validation = joi.object(),

        initialValues = {},
        initialErrors = {},
        disabledFields = []
    } = {}) {
        let emitter = createEmitter();
        this._emitter = emitter;
        this.on = emitter.on;
        this.off = emitter.off;
        this.once = emitter.once;
        this.emit = emitter.emit;

        this.name = name;

        this.#initialValues = clone(initialValues || {});
        this.#initialErrors = clone(initialErrors || {});
        this.#values = clone(initialValues || {});
        this.#disabled = clone(disabledFields);
        this.validateOnBlur = validateOnBlur;
        this.validation = validation;

        if(onSubmit) this.on("submit", onSubmit);
        if(onChange) this.on("change", onChange);
        if(onError) this.on("errors", onError);
        if(onUpdate) this.on("update", onUpdate);

        this.Form = setupForm(this);
        this.Fields = setupFields(this);
        this.FormObject = FormObject;
        this.FieldWatcher = setupFieldWatcher(this);

        if(debugUpdates) this.on("update", console.log.bind(console, `${name}-update`));
        if(debugChanges) this.on("change", console.log.bind(console, `${name}-change`));

        let logic = new LogicBindings({
            getError: field => get(this.#errors, field),
            getValue: field => get(this.#values, field),

            setError: (field, error, update) => set(this.#errors, field, error),
            setValue: ({ field, value, previousValue, callback, update }) => {
                set(this.#values, field, value);

                if(update) {
                    this.change({ field, previousValue, callback: typeof callback === "function" ? callback : undefined });
                }
            },

            delError: field => unset(this.#errors, field),
            delValue: field => unset(this.#values, field),

            hasValue: field => propertyExists(this.#values, field),
            isDisabled: field => this.#disabled.includes(field),

            validateOnBlur: () => this.validateOnBlur,
            validate: this.validate
        });

        this.#createOnChange = logic.createOnChange;
        this.#createOnBlur = logic.createOnBlur;
        this.hookField = logic.hookField;
    }

    #createOnChange = e => null;
    #createOnBlur = e => null;

    /**
     * @param {...Function} listeners
     */
    onChange = (...listeners) => {
        if(typeof listeners[0] === "string" && listeners.length > 1) {
            // On change specific field
            this.on(`change-${listeners[0]}`, ...listeners.slice(1));
        } else {
            // Generalized on change
            this.on("change", ...listeners);
        }
    }

    /**
     * @param {...Function} listeners
     */
    offChange = (...listeners) => {
        if(typeof listeners[0] === "string" && listeners.length > 1) {
            // On change specific field
            this.off(`change-${listeners[0]}`, ...listeners.slice(1));
        } else {
            // Generalized on change
            this.off("change", ...listeners);
        }
    };

    /**
     * @param {...Function} listeners
     */
    onSubmit = (...listeners) => {
        this.on("submit", ...listeners);
    };

    /**
     * @param {...Function} listeners
     */
    offSubmit = (...listeners) => {
        this.off("submit", ...listeners);
    };

    addField = target => {
        if(!this.#fields.includes(target))
            this.#fields.push(target);
    };

    removeField = target => {
        this.#fields.splice(this.#fields.indexOf(target), 1);
    };

    disableField = field => {
        if(!this.#disabled.includes(field)) {
            this.#disabled.push(field);
            this.change({ field, forceUpdate: true, disabled: true });
        }
    };

    enableField = field => {
        if(this.#disabled.includes(field)) {
            this.#disabled.splice(this.#disabled.indexOf(field), 1);
            this.change({ field, forceUpdate: true, disabled: false });
        }
    };

    disableFields = (...fields) => fields.forEach(field => this.disableField(field));

    enableFields = (...fields) => fields.forEach(field => this.enableField(field));

    setDisabled = (...fields) => {
        let targets = uniq([...this.#disabled, ...fields]);
        this.#disabled = fields;

        targets.forEach(field => this.change({ field }));
    };

    setEnabled = (...fields) => {
        let targets = uniq([...this.#disabled, ...fields]);
        this.#disabled = this.#disabled.filter(x => !fields.includes(x));

        targets.forEach(field => this.change({ field }));
    };

    disable = () => {
        let targets = uniq([...this.#disabled, ...this.#fields]);
        this.#disabled = [...this.#fields];

        targets.forEach(field => this.change({ field, forceUpdate: true, disabled: true }));
    }

    enable = () => {
        let targets = uniq([...this.#disabled, ...this.#fields]);
        this.#disabled = [];

        targets.forEach(field => this.change({ field, forceUpdate: true, disabled: false }));
    }

    getState = () => ({
        errors: this.#errors,
        values: this.#values
    });

    values = (...fields) => {
        if(fields.length === 0) {
            return clone(this.#values);
        } else if(fields.length === 1 && typeof fields[0] === "object") {
            Object.keys(fields[0]).map(field => this.value(field, fields[0][field], false));
        } else return fields.map(x => clone(get(this.#values, x)));
    };

    errors = (...fields) => {
        if(fields.length === 0) {
            return clone(this.#errors);
        } else if(fields.length === 1 && typeof fields[0] === "object") {
            Object.keys(fields[0]).map(field => this.error(field, fields[0][field]));
        } else return fields.map(x => get(this.#errors, x));
    };

    value = (field, value, validate = true) => {
        if(value !== undefined) {
            let previousValue = clone(get(this.#values, field));
            set(this.#values, field, value);

            if(validate) {
                this.validate(field, () => this.change({ field, previousValue }));
            } else {
                this.change({ field, previousValue });
            }
        } else return clone(get(this.#values, field));
    }

    error = (field, error, originalField = field) => {
        if(error !== undefined) {
            let exists = propertyExists(this.#errors, field);

            if(error && !exists) {
                set(this.#errors, field, error);
                this.change({ field: originalField });
            } else if(!error && exists) {
                unset(this.#errors, field);
                this.change({ field: originalField });
            }
        } else return get(this.#errors, field);
    }

    // These two should be used only in "./Form" to replace initial values/errors on mount
    replaceValues = (obj, initial = false) => {
        this.#values = clone(obj);

        if(initial) {
            this.#initialValues = clone(obj);
        }
    }

    replaceErrors = (obj, initial = false) => {
        this.#errors = clone(obj);

        if(initial) {
            this.#initialErrors = clone(obj);
        }
    }

    resetValues = () => this.values(clone(this.#initialValues));
    resetErrors = () => this.errors(clone(this.#initialErrors));
    resetForm = () => {
        this.#values = clone(this.#initialValues);
        this.#errors = clone(this.#initialErrors);
        this.#disabled = [];

        this.#fields.map(field => this.change({ field }));
    };

    resetFields = (...fields) => {
        for(let field of fields) {
            this.value(field, get(this.#initialValues, field));
        }
    }

    setState = (values = this.#values, errors = this.#errors, disabledFields = this.#disabled) => {
        this.#values = clone(values);
        this.#errors = clone(errors);
        this.#disabled = clone(disabledFields);

        this.#fields.map(field => this.change({ field }));
    }

    /**
     * @param {string} field
     */
    validate = async(field, next = () => null, index) => {
        if(!field) {
            let { error, value } = await this.validateForm();

            if(!error) {
                this.emit("submit", value, this);
            }
        } else {
            if(!this.validateOnBlur) return next(null, field);

            let { error } = await validateForm(this, this.#values);

            if(error && error[field]) {
                let err;

                if(index !== undefined) {
                    err = error[field][index];
                    this.error(`${field}[${index}]`, err, field);
                } else {
                    err = error[field];
                    this.error(field, err);
                }

                return next(err, field);
            }

            if(index !== undefined) {
                this.error(`${field}[${index}]`, null, field);
                return next(null, field);
            }

            this.error(field, null);
            return next(null, field);
        }
    }

    validateForm = async() => {
        let { error, value } = await validateForm(this, this.#values);
        this.#errors = {};

        if(error) {
            this.#errors = error;
        }

        this.emit("errors", this.#errors, this);
        this.emit("update", { errors: this.#errors, values: this.#values }, this);

        this.#fields.forEach(field => this.update({ field }));

        return { error, value };
    }

    /**
     * @param {string} field
     */
    change = ({ field, callback, previousValue, forceUpdate = false, disabled }) => {
        let value = get(this.#values, field);

        let payload = {
            field,
            value: value,
            previousValue: previousValue !== undefined ? previousValue : value,
            error: get(this.#errors, field),
            forceUpdate,
            disabled
        };

        this.emit("change", { ...payload }, this);
        this.emit("update", { errors: this.#errors, values: this.#values, forceUpdate }, this);
        this.update(payload);

        if(callback) callback(field, payload);
    };

    /**
     * @param {string} field
     */
    update = ({ field, previousValue = get(this.#values, field) }) =>
        this.emit(`change-${field}`, { value: get(this.#values, field), previousValue, error: get(this.#errors, field) }, this);

    updateFields = () => {
        this.#fields.forEach(field => this.change({ field, forceUpdate: true }));
    }

    hookSubmit = async(e) => {
        if(e && e.preventDefault) e.preventDefault();
        return await this.validate();
    };
}
