import React from "react";

/**
 * Use refs with function components by accessing ref as a prop rather than a second param
 * @template T
 * @param {React.Component<T, any>} Component
 * @returns {React.Component<T, any>}
 */
export default Component => React.forwardRef((props, ref) => (
    <Component {...props} forwardedRef={ref} />
));