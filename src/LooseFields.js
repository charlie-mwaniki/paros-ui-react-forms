//? LooseField - Use the convenient parts of ReactForm like templates, without ReactForm.
//? They are still bloated components with overhead, but super convenient!

import React from "react";
import withRef from "./hoc/withRef";

import { getHookTemplate, IS_ARRAY_ADAPTER } from "./utils";
import MetadataContext from "./MetadataContext";
import LogicBindings from "./logic";

const IGNORED_PROPS = [
    "_cachedFields", "form"
];

const LooseField = (prop, config) => class LooseField extends React.PureComponent {
    state = { definition: {} };

    static contextType = MetadataContext;

    render() {
        let { Component, ref, template, props } = getHookTemplate(prop, this.context, this.props);
        let { hidden, ...hook } = config.bindings.getLooseProps(template);

        if(hidden) return null;

        if(Component[IS_ARRAY_ADAPTER]) {
            return (
                <Component {...hook} {...props} template={template} hook={hook} ref={ref} form={null} />
            );
        } else {
            return (
                <Component {...hook} {...props} ref={ref} />
            );
        }
    }
};

class LooseFields {
    _cachedFields = {};

    config = {};

    constructor(config = {}) {
        this.config = { ...config };

        if(!this.config.bindings) {
            this.config.bindings = new LogicBindings({});
        }
    }

    // LooseFields, "company"
    get(self, prop) {
        if(IGNORED_PROPS.includes(prop)) return self[prop];

        if(!this._cachedFields[prop]) {
            this._cachedFields[prop] = withRef(LooseField(prop, this.config));
        }

        return this._cachedFields[prop];
    }
}

/**
 * @returns {ReactForm.FormFields}
 */
const setup = config => {
    let fields = new LooseFields(config);
    return new Proxy(fields, fields);
};

export const withLooseFields = config => {
    let [ fields ] = React.useState(React.useMemo(() => setup(config), []), []);
    return fields;
};

export default setup;
